<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantSocialProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('applicant_social_profiles', function(Blueprint $table)
		{
			$table->unsignedInteger('applicant_id');
			$table->unsignedInteger('profile_id');
			$table->string('link');
	        //$table->rememberToken();
	        //$table->timestamps();

	        $table->foreign('applicant_id')->references('id')->on('applicants');
	        $table->foreign('profile_id')->references('id')->on('social_profiles');
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('applicant_social_profiles');
	}

}
