<?php

use Illuminate\Database\Seeder;

class SocialProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //delete table records
        DB::table('social_profiles')->delete();
        //insert some dummy records
        DB::table('social_profiles')->insert(array(
           array(
            'name'        =>'Facebook',
            'domain_name' =>'www.facebook.com'
            ),
           array(
            'name'        =>'Instagram',
            'domain_name' =>'www.instagram.com'
            ),
           array(
            'name'        =>'LinkedIn',
            'domain_name' =>'www.linkedin.com'
            )
        ));
    }
}
