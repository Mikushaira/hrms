<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantAnswersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('applicant_answers', function(Blueprint $table)
		{
			$table->unsignedInteger('job_id');
			$table->unsignedInteger('applicant_id');
			$table->unsignedInteger('question_id');
			$table->string('answer');

			$table->primary(['job_id', 'applicant_id','question_id']);

	        //$table->rememberToken();
	        //$table->timestamps();

	        $table->foreign('job_id')->references('id')->on('job_posts');
	        $table->foreign('applicant_id')->references('id')->on('applicants');
	        $table->foreign('question_id')->references('id')->on('questions');
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('applicant_answers');
	}

}
