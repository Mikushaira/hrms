<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //delete table records
        DB::table('companies')->delete();
        //insert some dummy records
        DB::table('companies')->insert(array(
           array(
            'name' => 'Floss',
            'about' => 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.'
            ),
           array(
            'name' => 'Mesasix',
            'companies' => 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.'
            ),
           array(
            'name' => 'DLA Piper',
            'companies' => 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.'
            )
        ));
    }
}
