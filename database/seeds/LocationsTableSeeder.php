<?php

use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         DB::table('locations')->delete();
        //insert some dummy records
        DB::table('locations')->insert(array(
           array(
            'street'  => 'Quezon Avenue Extension',
            'city'    => 'Iligan',
            'state'   => 'Lanao Del Norte',
            'country' => 'Philippines',
            'zip'     => '9200'
            ),
           array(
            'street'  => '2430 Victory Park Lane',
            'city'    => 'Dallas',
            'state'   => 'Texas',
            'country' => 'USA',
            'zip'     => '75219 '
            ),
           array(
            'street'  => 'P.O. Box 769317',
            'city'    => 'Khalifa Park',
            'state'   => 'Abu Dhabi',
            'country' => 'UAE',
            'zip'     => '769317'
            ),

        ));
    }
}
