<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_locations', function(Blueprint $table)
		{
			$table->unsignedInteger('user_id');
			$table->unsignedInteger('location_id');

			$table->primary(['user_id', 'location_id']);

	        //$table->rememberToken();
	        //$table->timestamps();

	        $table->foreign('user_id')->references('id')->on('users');
	        $table->foreign('location_id')->references('id')->on('locations');
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_locations');
	}

}
