<?php

use Illuminate\Database\Seeder;

class ApplicantAnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //delete table records
        DB::table('applicant_answers')->delete();
        //insert some dummy records
        DB::table('applicant_answers')->insert(array(
           array(
            'job_id'       => '1',
            'applicant_id' => '1',
            'question_id'  => '1',
            'answer'       => 'Lorem ipsum.'
            ),
           array(
            'job_id'       => '2',
            'applicant_id' => '2',
            'question_id'  => '2',
            'answer'       => 'Lorem ipsum.'
            ),
           array(
            'job_id'       => '3',
            'applicant_id' => '2',
            'question_id'  => '3',
            'answer'       => 'Lorem ipsum.'
            )
        ));
    }
}

