<?php

use Illuminate\Database\Seeder;

class ApplicantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //delete table records
        DB::table('applicants')->delete();
        //insert some dummy records
        DB::table('applicants')->insert(array(
           array(
           	'fname'          => 'Jon',
            'lname'          => 'Doe',
            'email'          => 'jondoe@gmail.com',
            'phone'          => '09767676767',
            'address'        => '062 Hidden Street, USA',
            'gender'         => 'Male',
            'birthdate'      => '01-01-1998',
            'cover_letter'   => 'Lorem Ipsum.',
            'identification' => 'path/to/file',
            'resume'         => 'path/to/file',
            'status_id'      => '3',
            'job_id'         => '1'
            ),
           array(
            'fname'          => 'Jannine',
            'lname'          => 'Smith',
            'email'          => 'jannnine@yahoo.com',
            'phone'          => '09876263771',
            'address'        => '111 Somewhere, USA',
            'gender'         => 'Female',
            'birthdate'      => '02-19-1980',
            'cover_letter'   => 'Lorem Ipsum.',
            'identification' => 'path/to/file',
            'resume'         => 'path/to/file',
            'status_id'      => '2',
            'job_id'         => '1'
            )
        ));
    }
}
