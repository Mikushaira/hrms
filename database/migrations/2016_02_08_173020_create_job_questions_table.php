<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobQuestionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('job_questions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('question_id');
			$table->unsignedInteger('job_id');
			$table->string('answer_type');

	        //$table->rememberToken();
	        //$table->timestamps();

	        $table->foreign('question_id')->references('id')->on('questions');
	        $table->foreign('job_id')->references('id')->on('job_posts');
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('job_questions');
	}

}
