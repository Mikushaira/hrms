<?php

use Illuminate\Database\Seeder;

class UserLocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         //
        DB::table('user_locations')->delete();
        //insert some dummy records
        DB::table('user_locations')->insert(array(
           array(
            'location_id' => '1',
            'user_id'     => '1'
            ),
            array(
            'location_id' => '2',
            'user_id'     => '2'
            ),
             array(
            'location_id' => '3',
            'user_id'     => '3'
            )
        ));
    }
}
