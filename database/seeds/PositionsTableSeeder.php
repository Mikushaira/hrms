<?php

use Illuminate\Database\Seeder;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->delete();
        //insert some dummy records
        DB::table('positions')->insert(array(
           array(
            'title' => 'Web Developer',
            'desc'  => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.'
            ),
            array(
            'title' => 'SEO',
            'desc'  => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.'
            ),
             array(
            'title' => 'Back-End Developer',
            'desc'  => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.'
            ),
            array(
            'title' => 'Nurse',
            'desc'  => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.'
            )
        ));
    }
}
