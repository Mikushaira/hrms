<?php

use Illuminate\Database\Seeder;

class JobQuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //delete table records
        DB::table('job_questions')->delete();
        //insert some dummy records
        DB::table('job_questions')->insert(array(
           array(
            'question_id' => '1',
            'job_id'      => '1',
            'answer_type' => 'video'
            ),
           array(
            'question_id' => '2',
            'job_id'      => '2',            
            'answer_type' => 'text'
            ),
           array(
            'question_id' => '3',
            'job_id'      => '3',
            'answer_type' => 'file'
            )
          
        ));
    }
}
