<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobPostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('job_posts', function(Blueprint $table)
		{
			$table->increments('id');
	        $table->string('title');
	        $table->string('desc');
	        $table->string('type');
	        $table->string('status');
	        $table->unsignedInteger('location_id');
	        $table->unsignedInteger('position_id');
	        $table->unsignedInteger('category_id');
	        //$table->rememberToken();
	        //$table->timestamps();

	        $table->foreign('location_id')->references('id')->on('locations');
	        $table->foreign('position_id')->references('id')->on('positions');
	        $table->foreign('category_id')->references('id')->on('categories');
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('job_posts');
	}

}
