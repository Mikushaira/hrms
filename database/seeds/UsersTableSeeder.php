<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         DB::table('users')->delete();
        //insert some dummy records
        DB::table('users')->insert(array(
           array(
            'fname'      => 'Inday',
            'lname'      => 'Bote',
            'email'      => 'inday@gmail.com',
            'password'   => 'botilya',
            'role_id'    => '1',
            'company_id' => '1'
            ),
           array(
            'fname'      => 'Pedro',
            'lname'      => 'Penduko',
            'email'      => 'pendroPends@gmail.com',
            'password'   => 'kwintas',
            'role_id'    => '2',
            'company_id' => '2'
            ),
           array(
            'fname'      => 'Maria',
            'lname'      => 'Makiling',
            'email'      => 'maria@gmail.com',
            'password'   => 'baguio',
            'role_id'    => '1',
            'company_id' => '3'
            )
        ));
        
    }
}
