<?php

use Illuminate\Database\Seeder;

class ApplicantSocialProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //delete table records
        DB::table('applicant_social_profiles')->delete();
        //insert some dummy records
        DB::table('applicant_social_profiles')->insert(array(
           array(
           	'applicant_id' => '1',
            'profile_id'   => '1',
            'link'         => 'https://www.facebook.com/rodzainna.hamisain'
            ),
           array(
            'applicant_id' => '1',
            'profile_id'   => '1',
            'link'         => 'https://www.facebook.com/shaira.masanting'
            )
        ));
    }
}
