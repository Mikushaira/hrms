<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //delete table records
        DB::table('categories')->delete();
        //insert some dummy records
        DB::table('categories')->insert(array(
           array(
            'name' => 'Medical',
            'desc' => 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.'
            ),
           array(
            'name' => 'IT',
            'desc' => 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.'
            ),
           array(
            'name' => 'Law',
            'desc' => 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.'
            )
        ));
    }
}
