<?php

use Illuminate\Database\Seeder;

class JobPostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('job_posts')->delete();
        //insert some dummy records
        DB::table('job_posts')->insert(array(
           array(
            'title'       => 'Nurse',
            'desc'        => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.',
            'type'        => 'Full Time',
            'status'      => 'Published',
            'location_id' => '3',
            'position_id' => '4',
            'category_id' => '1'
            ),
            array(
             'title'       => 'Web Developer',
             'desc'        => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.',
             'type'        => 'Part Time',
             'status'      => 'Approved',
             'location_id' => '2',
             'position_id' => '1',
             'category_id' => '2'
            ),
             array(
              'title'       => 'SEO',
              'desc'        => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.',
              'type'        => 'OJT',
              'status'      => 'Pending',
              'location_id' => '1',
              'position_id' => '2',
              'category_id' => '2'
            ),
        ));
    }
}
