<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employees', function(Blueprint $table)
		{
			$table->increments('id');
	        $table->string('fname');
	        $table->string('lname');
	        $table->string('email')->unique();
	        $table->string('phone');
	        $table->string('address');
	        $table->string('gender');
	        $table->string('birthdate');
	        $table->string('resume');
	        $table->string('type');
	        $table->string('status');
	        $table->unsignedInteger('user_location_id');
	        $table->unsignedInteger('position_id');
	        $table->unsignedInteger('job_id');
	        //$table->rememberToken();
	        //$table->timestamps();

	        $table->foreign('user_location_id')->references('user_id')->on('user_locations')
	        								   ->references('location_id')->on('user_locations');
	        $table->foreign('position_id')->references('id')->on('positions');
	        $table->foreign('job_id')->references('id')->on('job_posts');
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employees');
	}

}
