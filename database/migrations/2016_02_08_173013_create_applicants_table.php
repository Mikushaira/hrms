<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('applicants', function(Blueprint $table)
		{
			$table->increments('id');
	        $table->string('fname');
	        $table->string('lname');
	        $table->string('email')->unique();
	        $table->string('phone');
	        $table->string('address');
	        $table->string('gender');
	        $table->string('birthdate');
	        $table->string('cover_letter');
	        $table->string('identification');
	        $table->string('resume');
	        $table->unsignedInteger('status_id');
	        $table->unsignedInteger('job_id');
	        //$table->rememberToken();
	        //$table->timestamps();

	        $table->foreign('status_id')->references('id')->on('applicant_statuses');
	        $table->foreign('job_id')->references('id')->on('job_posts');
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('applicants');
	}

}
