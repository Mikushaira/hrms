<?php

use Illuminate\Database\Seeder;

class ApplicantStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //delete table records
        DB::table('applicant_statuses')->delete();
        //insert some dummy records
        DB::table('applicant_statuses')->insert(array(
           array(
            'status' =>'hired'
            ),
           array(
            'status' =>'on-board'
            ),
           array(
            'status' =>'interviewing'
            )
        ));
    }
}
