<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //delete table records
        DB::table('employees')->delete();
        //insert some dummy records
        DB::table('employees')->insert(array(
           array(
           	'fname'            => 'Rodzainna',
            'lname'            => 'Hamisain',
            'email'            => 'rodzainna@gmail.com',
            'phone'            => '09061420260',
            'address'          => '062 Jamille Street, PH',
            'gender'           => 'Female',
            'birthdate'        => '01-15-1996',
            'resume'           => 'path/to/file',
            'type'             => 'Part Time',
            'status'           => 'on-leave',
            'user_location_id' => '3',
            'position_id'      => '1',
            'job_id'           => '1'
            ),
           array(
            'fname'            => 'Miku Shaira',
            'lname'            => 'Masanting',
            'email'            => 'mikushaira@gmail.com',
            'phone'            => '09111111111',
            'address'          => 'Tambo, PH',
            'gender'           => 'Female',
            'birthdate'        => '01-10-1996',
            'resume'           => 'path/to/file',
            'type'             => 'Full Time',
            'status'           => 'hired',
            'user_location_id' => '3',
            'position_id'      => '2',
            'job_id'           => '1'
            )
        ));
    }
}
