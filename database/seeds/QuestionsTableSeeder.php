<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('questions')->delete();
        //insert some dummy records
        DB::table('questions')->insert(array(
           array(
            'question'    => 'What Are Your Weaknesses?',
            'category_id' => '1'
            ),
           array(
            'question'    => 'What development tools have you used?',
            'category_id' => '2'
            ), 
           array(
            'question'    => 'How do you think the firm will grow in the next five years?',
            'category_id' => '3'
            )
        ));
    }
}
