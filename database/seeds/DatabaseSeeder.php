<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(ApplicantStatusesTableSeeder::class);
        $this->call(SocialProfilesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(PositionsTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(LocationsTableSeeder::class);
        $this->call(QuestionsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(UserLocationsTableSeeder::class);
        $this->call(JobPostsTableSeeder::class);
        $this->call(EmployeesTableSeeder::class);
        $this->call(ApplicantsTableSeeder::class);
        $this->call(ApplicantSocialProfilesTableSeeder::class);
        $this->call(JobQuestionsTableSeeder::class);
        $this->call(ApplicantAnswersTableSeeder::class);
    }
}
